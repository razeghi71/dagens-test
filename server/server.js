const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors')
const products = require('./db');
const { v4: uuidv4 } = require('uuid');

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const validateProduct = (product) => {
  const errors = [];

  if (!product.name || typeof product.name !== 'string' || product.name.trim() === '') {
    errors.push('Name is required and must be a string.');
  }

  const validCategories = ['meat', 'greens', 'fish'];
  if (!product.category || !validCategories.includes(product.category)) {
    errors.push('Category is invalid. Must be one of the following: meat, greens, fish.');
  }

  if (product.price === undefined || isNaN(Number(product.price)) || Number(product.price) <= 0) {
    errors.push('Price is required and must be a positive number.');
  } else {
    product.price = Number(product.price);
  }

  return errors.length > 0 ? errors : null;
};


http.createServer(app).listen(3001, () => {
  console.log('Listen on 0.0.0.0:3001');
});

app.get('/', (_, res) => {
  res.send({ status: 200 });
});

app.post('/add', async (req, res) => {
  const validationErrors = validateProduct(req.body);
  if (validationErrors) {
    return res.status(400).json({ statusCode: 400, message: 'Bad product', errors: validationErrors });
  } else {
    const id = uuidv4();
    const productWithId = { id, ...req.body };
    products.push(productWithId);
    res.status(201).json({ statusCode: 201, message: "Success" });
  }
});

app.get('/get', async (req, res) => {
  const { max_price, min_price, category, page = "1" } = req.query;

  const pageNumber = parseInt(page, 10);
  const pageSize = 24;

  // some validation and error handling here

  let filteredProducts = products.filter((product) => {
    return (
      (!max_price || product.price <= max_price) &&
      (!min_price || product.price >= min_price) &&
      (!category || product.category === category)
    );
  });

  const startIndex = (pageNumber - 1) * pageSize;
  const endIndex = pageNumber * pageSize;

  const paginatedProducts = filteredProducts.slice(startIndex, endIndex);

  res.json({
    page: pageNumber,
    pageSize: pageSize,
    total: filteredProducts.length,
    data: paginatedProducts
  });
});

const findNearestPricedProducts = (productId, n) => {
  const product = products.find(p => p.id === productId);
  if (!product) return null;

  const sameCategoryProducts = products.filter(p => p.id !== productId && p.category === product.category);

  sameCategoryProducts.sort((a, b) => {
    return Math.abs(a.price - product.price) - Math.abs(b.price - product.price);
  });

  return sameCategoryProducts.slice(0, n);
};


app.get('/nearest-prices/:id', (req, res) => {
  const { id } = req.params;
  const N = 5;

  const nearestProducts = findNearestPricedProducts(id, N);

  if (nearestProducts) {
    res.json(nearestProducts);
  } else {
    res.status(404).json({ message: 'Product not found or no products in the same category.' });
  }
});


process.on('SIGINT', function () {
  process.exit();
});
