import { useState } from 'react';

const App = () => {

  const [product, setProduct] = useState({
    name: '',
    category: 'meat',
    price: ''
  });

  const [submitStatus, setSubmitStatus] = useState(null);

  const onFormSubmit = (e) => {
    e.preventDefault();
    fetch('//127.0.0.1:3001/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(product),
    })
      .then(response => response.json())
      .then(data => {
          if (data.statusCode === 201) {
            setSubmitStatus('Product added successfully!');
          } else {
            let message = <div>{data.message}</div>;
            if (data.errors) {
              const errorsList = (
                <ul>
                  {data.errors.map((error, index) => (
                    <li key={index}>{error}</li>
                  ))}
                </ul>
              );
              message = (
                <div>
                  {message}
                  {errorsList}
                </div>
              );
            }
            setSubmitStatus(message);
          }
        }
      )
      .catch(() => setSubmitStatus('An error occurred while adding the product.'));
  }

  const handleChange = (e) => {
    setProduct({ ...product, [e.target.name]: e.target.value });
  }

  return (
    <form onSubmit={onFormSubmit}>
      <div>
        <label>Name:</label>
        <input
          type="text"
          name="name"
          value={product.name}
          onChange={handleChange}
        />
      </div>
      <div>
        <label>Category:</label>
        <select name="category" value={product.category} onChange={handleChange}>
          <option value="meat">Meat</option>
          <option value="greens">Greens</option>
          <option value="fish">Fish</option>
        </select>
      </div>
      <div>
        <label>Price:</label>
        <input
          type="number"
          name="price"
          value={product.price}
          onChange={handleChange}
        />
      </div>
      <button type="submit">Submit</button>
      {submitStatus && <div>{submitStatus}</div>}
    </form>
  );

}

export default App;
